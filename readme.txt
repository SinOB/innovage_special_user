=== InnovAge_Special User ===
Contributors: SinOB
Tags: buddypress
Requires at least: WP 3.9, BuddyPress 2.0.1
Tested up to: WP 4.0.1, BuddyPress 2.0.3
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Allows a logged in user to create a 'fake' user account for another person. Will also
join the user to the specified group and create a dyad between the two users.
Relies on the presence of innovage_pedometer plugin.


== Description ==

Innovage Special User is a plugin that has been designed to allow a logged in user
to create a user account for someone who will never actually use the site. This
will have limited use for anything outside of the iStep website. On iStep this 
will be used in the case where we have a person with technical skills who 
wants to do a partner challenge with a person who doesn't have internet access or
technical skills. The technical user can create the account for their partner and
manage their steps. The non technical user can still do the challenge but does not
have to learn how to use the site.

When the technical user creates the account, the account is automatically added 
to the same group and partnered with the creator user. Requires innovage_pedometer
to function fully.

There are currently no admin options.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
