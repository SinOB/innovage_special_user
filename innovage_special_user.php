<?php

/*
  Plugin Name: Innovage Special User
  Plugin URI: https://bitbucket.org/SinOB/innovage_special_user
  Description: Allow a logged in user to create a new special user, join that 
   user to the same group and form apartnership between both
  Version: 1.0
  Author: Sinead O'Brien 
  Author URI: https://bitbucket.org/SinOB
  Requires at least: 3.9
  Tested up to: WP 4.0.1, BuddyPress 2.0.1
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

$innovage_special_user_error = new WP_Error();
add_shortcode('innovage_special_user_form', 'innovage_special_user_form');

/** /
 * Allow the user to fill in the details of a new user to partner up with
 *
 * @param type $atts
 * @param type $content
 */
function innovage_special_user_form($atts, $content = '') {
    $current_user = bp_loggedin_user_id();

    // Post the group id to the page
    if (!isset($_POST['group_id']) || empty($_POST['group_id'])) {
        echo "An error has occured. No group id has been provided.";
        return;
    }

    $group_id = $_POST['group_id'];

    if (!empty($_POST) && isset($_POST['create_user'])) {
        // if successfully save the form do not show form again
        if (innovage_special_user_save($_POST, $group_id)) {
            return;
        }
    }

    // User must be a member of the group to continue
    if (!groups_is_user_member($current_user, $group_id)) {
        echo "<p>You must be a member of a group before you can use this "
        . "feature. Please return to the dashboard and join a group.</p>";
        return;
    }

    // User must not already have a partner in the group to continue
    $partner_team_info = innovage_partner_get_group_partner($current_user, $group_id);
    if (isset($partner_team_info) && !empty($partner_team_info)) {
        echo "<p>This tool is only available to people who do not already have a "
        . "partner. Please unpartner from any existing partners in order to use "
        . "this tool.</p>";
        return;
    }

    include 'innovage_special_user_form.php';
}

function innovage_special_user_save($postdata, $group_id) {
    global $innovage_special_user_error, $wpdb;

    $postdata = innovage_special_user_sanatise_user_save($postdata);

    // Only continue with save if there are no errors
    if (!empty($innovage_special_user_error) && count($innovage_special_user_error->errors) > 0) {
        innovage_special_user_printErrorMessages($innovage_special_user_error);
        return false;
    }

    // Generate the password and create the user
    $current_user_email = bp_core_get_user_email(get_current_user_id());
    $password = wp_generate_password(12, false);
    $username = $postdata['special_user_username'];
    $username = preg_replace('/\s+/', '', sanitize_user($username, true));
    $email_address = innovage_special_user_get_email($current_user_email);
    $year_of_birth = $postdata['special_user_year_of_birth'];
    $gender = $postdata['special_user_gender'];
    $displayname = sanitize_title($username);

    // Create the user meta
    $usermeta = array();
    $usermeta['field_1'] = $displayname;
    $usermeta['password'] = $password;
    $usermeta['nickname'] = $displayname;

    // Create the user via buddypress
    $result = BP_Signup::add_backcompat($username, $password, $email_address, $usermeta);

    if (is_wp_error($result)) {
        innovage_special_user_printErrorMessages($result);
        return false;
    } else {
        $new_user_id = $result;

        // set the nickname to be the display name
        update_user_meta($new_user_id, 'nickname', $displayname);

        // Set the new user xprofile year of birth
        xprofile_set_field_data('Year of birth', $new_user_id, $year_of_birth, true);

        // Set the new user gender
        $blob = xprofile_set_field_data('Gender', $new_user_id, $gender, true);

        $activation_key = wp_hash($new_user_id);
        update_user_meta($new_user_id, 'activation_key', $activation_key);

        $args = array(
            'user_login' => $username,
            'user_email' => $email_address,
            'activation_key' => $activation_key,
            'meta' => $usermeta,
        );

        BP_Signup::add($args);

        // this should activate the account through buddypress
        // and send admin new user email notification
        apply_filters('bp_core_activate_account', bp_core_activate_signup($activation_key));


        BP_Signup::validate($activation_key);
        // finish activation and set display name appropriately
        // this will activate the account wp account even if for any reason 
        // buddypress activation failed
        $wpdb->query($wpdb->prepare("UPDATE $wpdb->users SET display_name=%s, "
                        . "user_status=0 WHERE ID=%d", $displayname, $new_user_id));

        // Have the member join the group
        groups_accept_invite($new_user_id, $group_id);

        // Create the partnership between the current user and new user
        innovage_partnership_save($new_user_id, $group_id);

        // Email the user
        $subject = 'Special User Account Created on iStep';
        $message = 'You have created a special user account on iStep with the following details;'
                . ' Username: ' . $username
                . ' Password: ' . $password;

        wp_mail($current_user_email, $subject, $message);

        echo "Thank you. A special user has been successfully created and partnered with your account.";
        return true;
    }
}

function innovage_special_user_sanatise_user_save($postdata) {
    global $innovage_special_user_error;

    // Make sure year of birth is int and >4 digits long
    $postdata['special_user_year_of_birth'] = intval($postdata['special_user_year_of_birth']);
    if ($postdata['special_user_year_of_birth'] > 9999 || $postdata['special_user_year_of_birth'] < 999) {
        $innovage_special_user_error->add('required', __('The year of birth is a required field and must be 4 digits long.'));
    }

    // Make sure username is clean and exists
    $postdata['special_user_username'] = sanitize_user($postdata['special_user_username'], true);
    if (!isset($postdata['special_user_username']) || empty($postdata['special_user_username'])) {
        $innovage_special_user_error->add('required', __('The username is required and must be unique.'));
    }

    if (!isset($postdata['confirm_responsible']) || $postdata['confirm_responsible'] != 1) {
        $innovage_special_user_error->add('required', __('You must check the checkbox to confirm that you take full responsibility for keeping this account up to date.'));
    }

    return $postdata;
}

/** /
 * Function accepts email of existing user and combines it with the unix timestamp
 * to create a unique but identifable address for special user accounts
 * 
 * @param type $base_email
 * @return string
 */
function innovage_special_user_get_email($base_email) {
    $email = 'iStepSpecial' . time() . '_' . $base_email;
    return $email;
}

/** /
 * Print error messages if any when try to save goal form
 * 
 * @param type $wp_errors
 */
function innovage_special_user_printErrorMessages($wp_errors) {
    if (is_wp_error($wp_errors)) {
        $errors = $wp_errors->get_error_messages();
        echo "<div id='message' class='innovage_error' >";
        echo "<strong>Error(s);</strong><br/>";
        foreach ($errors as $error) {
            echo $error . '<br/>';
        }
        echo "</div>";
    }
}
