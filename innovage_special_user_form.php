<?php
/*
 * Form used in creation of a special non registered user account.
 * This is for the case of an older user that has no internet access. 
 * The account is programatically created by wordpress with a faked emailaddress 
 * based on the creator emailaddress.
 * The new account is joined to the same bp group as the creator and partnered with the creator.
 * An account created in this way will be managed by their younger partner.
 * It is not expected that the new special user will ever access the site directly 
 * using an email address and password.
 * Current user is sent an email with the username and password for the special user account
 */
?>
<div class="register-section" id="profile-details-section">
    <form method="POST">
        <h2>Create Partner Managed Account</h2>
        <p>Create a special account for a non-web user</p>

        <p><label>Username (required)</label><br/>
            <input type='text' name='special_user_username' value='' required="required"/>
        </p>
        <p>
            <label>Year of birth (required)</label><br/>
            <input type='text' name='special_user_year_of_birth' value='' required="required"/>
        </p>
        <p><label>Gender (required)</label><br/>
            <select name="special_user_gender" required="required">
                <option value=""></option>
                <option value="Female">Female</option>
                <option value="Male">Male</option>

            </select> 
        </p><p>
            <input type="checkbox" name="confirm_responsible" value="1" required="required">
            I confirm that I will take full responsibility for keeping this account up to date.(required)
        </p>
        <input type='hidden' name='group_id' value='<?php echo $group_id ?>' />
        <input name='create_user' type="Submit" value="Create this user and make them my partner">
    </form>
</div>
